package com.mismisimos.sweetdreams.repository;

import com.mismisimos.sweetdreams.model.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Integer> {

    @Query("SELECT c from Cliente c WHERE c.rut = :rut")
    Cliente findByRut(@Param("rut") String rut);

}
