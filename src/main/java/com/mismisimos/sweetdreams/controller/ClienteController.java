package com.mismisimos.sweetdreams.controller;

import com.mismisimos.sweetdreams.model.Cliente;
import com.mismisimos.sweetdreams.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/clientes")
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    //Todos los clientes
    @GetMapping("/")
    public List<Cliente> getAllClientes(){
        return clienteService.listAll();
    }

    //Agregar cliente
    @PostMapping("/")
    public Cliente crearCliente(@RequestBody Cliente cliente){
        return clienteService.save(cliente);
    }

    //Obtener 1 cliente por id
    @GetMapping("/{id}")
    public Cliente getClienteById(@PathVariable(value = "id") int clienteID) {
        return clienteService.findById(clienteID);
    }

    //Obtener 1 cliente por rut
    @GetMapping("/rut/{rut}")
    public Cliente getClienteByRut(@PathVariable(value = "rut") String rutCliente){
        return clienteService.findByRut(rutCliente);
    }

    //Borrar cliente
    @DeleteMapping("/{id}")
    public void borrarCliente(@PathVariable(value = "id") int clienteRut) {
        clienteService.delete(clienteRut);
    }



}
