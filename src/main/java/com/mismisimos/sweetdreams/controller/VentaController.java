package com.mismisimos.sweetdreams.controller;

import com.mismisimos.sweetdreams.model.Venta;
import com.mismisimos.sweetdreams.service.VentaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/pedidos/ventas")
public class VentaController {

    @Autowired
    VentaService ventaService;

    @GetMapping("/")
    public List<Venta> getAllVentas(){
        return ventaService.listAll();
    }

    @PostMapping("/")
    public Venta crearVenta(@RequestBody Venta venta) {
        return ventaService.save(venta);
    }

    @GetMapping("/{id}")
    public Venta getPedidoById(@PathVariable(value = "id") int ventaBuscar) {
        return ventaService.findById(ventaBuscar);
    }

    @DeleteMapping("/{id}")
    public void borrarPedido(@PathVariable(value = "id") int ventaBuscar) {
        ventaService.delete(ventaBuscar);
    }

}
