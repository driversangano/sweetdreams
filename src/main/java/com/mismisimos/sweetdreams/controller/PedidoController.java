package com.mismisimos.sweetdreams.controller;

import com.mismisimos.sweetdreams.model.Pedido;
import com.mismisimos.sweetdreams.service.PedidoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/pedidos")
public class PedidoController {

    @Autowired
    PedidoService pedidoService;

    @GetMapping("/")
    public List<Pedido> getAllPedidos(){
        return pedidoService.listAll();
    }

    @PostMapping("/")
    public Pedido crearPedido(@RequestBody Pedido pedido) {
        return pedidoService.save(pedido);
    }

    @GetMapping("/{id}")
    public Pedido getPedidoById(@PathVariable(value = "id") int pedidoBuscar) {
        return pedidoService.findById(pedidoBuscar);
    }

    @DeleteMapping("/{id}")
    public void borrarPedido(@PathVariable(value = "id") int pedidoBuscar) {
        pedidoService.delete(pedidoBuscar);
    }

}
