package com.mismisimos.sweetdreams.controller;

import com.mismisimos.sweetdreams.model.Producto;
import com.mismisimos.sweetdreams.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/productos")
public class ProductoController {

    @Autowired
    ProductoService productoService;

    @GetMapping("/")
    public List<Producto> getAllProductos(){
        return productoService.listAll();
    }

    @PostMapping("/")
    public Producto crearProducto(@RequestBody Producto producto){
        return productoService.save(producto);
    }

    @GetMapping("/{id}")
    public Producto getProductoById(@PathVariable(value = "id") int productoBuscar) {
        return productoService.findById(productoBuscar);
    }


    @DeleteMapping("/{id}")
    public void borrarProducto(@PathVariable(value = "id") int productoBuscar) {
        productoService.delete(productoBuscar);
    }

}
