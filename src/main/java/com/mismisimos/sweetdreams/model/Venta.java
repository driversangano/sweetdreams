package com.mismisimos.sweetdreams.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table( name = "venta")
public class Venta implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer idv;

    @JsonBackReference(value="pedidoVent")
    @ManyToOne
    @JoinColumn(name = "idPedido")
    private Pedido pedido;

    @JsonBackReference(value="productoVent")
    @ManyToOne
    @JoinColumn(name = "idProducto")
    private Producto producto;

    private int cantidadProducto;

    private int precioVenta;

    public Venta() {
    }

    public Venta(Pedido pedido, Producto producto, int cantidadProducto, int precioVenta) {
        this.pedido = pedido;
        this.producto = producto;
        this.cantidadProducto = cantidadProducto;
        this.precioVenta = precioVenta;
    }

    public Integer getIdv() {
        return idv;
    }

    public void setIdv(Integer idv) {
        this.idv = idv;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public int getCantidadProducto() {
        return cantidadProducto;
    }

    public void setCantidadProducto(int cantidadProducto) {
        this.cantidadProducto = cantidadProducto;
    }

    public int getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(int precioVenta) {
        this.precioVenta = precioVenta;
    }
}
