package com.mismisimos.sweetdreams.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "pedido")
public class Pedido implements Serializable {

    private static final long serialVersionUID = 3310497689152374880L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idPedido;

    @JsonBackReference(value="clienteProd")
    @ManyToOne( fetch = FetchType.LAZY)
    private Cliente cliente;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime fechaEntrega;

    private int totalPedido;

    @JsonManagedReference(value="pedidoVent")
    @OneToMany(mappedBy = "pedido", fetch = FetchType.LAZY)
    @JsonIgnore
    Set<Venta> venta;

    public Pedido() {
    }

    public Pedido(Cliente cliente, LocalDateTime fechaEntrega, int totalPedido, Set<Venta> venta) {
        this.cliente = cliente;
        this.fechaEntrega = fechaEntrega;
        this.totalPedido = totalPedido;
        this.venta = venta;
    }

    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public LocalDateTime getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(LocalDateTime fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public Set<Venta> getVenta() {
        return venta;
    }

    public void setVenta(Set<Venta> venta) {
        this.venta = venta;
    }

    public int getTotalPedido() {
        return totalPedido;
    }

    public void setTotalPedido(int totalPedido) {
        this.totalPedido = totalPedido;
    }

    /*public void addVenta(Venta k){
        venta.add(k);
        k.setVenta(this);
    }

    public void delPedido(Venta k){
        venta.remove(k);
        k.setVenta(null);
    }*/

}
