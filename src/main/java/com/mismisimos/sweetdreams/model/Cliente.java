package com.mismisimos.sweetdreams.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "cliente")
@NamedQueries({
        @NamedQuery(name = "Cliente.findByRut",
                                query="SELECT c from Cliente c WHERE c.rut = :rut")
})
public class Cliente implements Serializable {

    private static final long serialVersionUID = -6891173017623693718L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_Cliente;
    //Restriccion

    private String nombre;
    private String apellido;
    private String correo;
    private String direccion;
    private String telefono;
    private String rut;

    @JsonManagedReference(value="clienteProd")
    @OneToMany(mappedBy = "cliente",
    fetch = FetchType.LAZY)
    @JsonIgnore
    Set<Pedido> pedido;

    public Cliente(){

    }

    public Cliente(String rut, String nombre, String apellido, String correo, String direccion, String telefono, Set<Pedido> pedido) {
        this.rut = rut;
        this.nombre = nombre;
        this.apellido = apellido;
        this.correo = correo;
        this.direccion = direccion;
        this.telefono = telefono;
        this.pedido = pedido;
    }

    public int getId_Cliente() {
        return id_Cliente;
    }

    public void setId_Cliente(int id_Cliente) {
        this.id_Cliente = id_Cliente;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Set<Pedido> getPedido() {
        return pedido;
    }

    public void setPedido(Set<Pedido> pedido) {
        this.pedido = pedido;
    }

    public void addPedido(Pedido k){
        pedido.add(k);
        k.setCliente(this);
    }

    public void delPedido(Pedido k){
        pedido.remove(k);
        k.setCliente(null);
    }


}
