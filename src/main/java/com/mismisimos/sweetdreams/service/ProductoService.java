package com.mismisimos.sweetdreams.service;

import com.mismisimos.sweetdreams.model.Producto;

import java.util.List;

public interface ProductoService {

    List<Producto> listAll();
    Producto save(Producto producto);
    Producto findById(int id);
    void delete(int id);

}
