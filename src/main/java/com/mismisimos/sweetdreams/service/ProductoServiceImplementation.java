package com.mismisimos.sweetdreams.service;

import com.mismisimos.sweetdreams.model.Producto;
import com.mismisimos.sweetdreams.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductoServiceImplementation implements ProductoService{

    @Autowired
    ProductoRepository productoRepository;

    @Override
    public List<Producto> listAll() {
        return productoRepository.findAll();
    }

    @Override
    public Producto save(Producto producto) {
        return productoRepository.save(producto);
    }

    @Override
    public Producto findById(int id) {
        return productoRepository.getOne(id);
    }

    @Override
    public void delete(int id) {
        productoRepository.deleteById(id);
    }
}
