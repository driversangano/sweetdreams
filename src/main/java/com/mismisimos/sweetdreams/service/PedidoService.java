package com.mismisimos.sweetdreams.service;

import com.mismisimos.sweetdreams.model.Pedido;

import java.util.List;

public interface PedidoService {

    List<Pedido> listAll();
    Pedido save(Pedido pedido);
    Pedido findById(int id);
    void delete(int id);

}
