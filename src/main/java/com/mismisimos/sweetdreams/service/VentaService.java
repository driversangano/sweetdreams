package com.mismisimos.sweetdreams.service;

import com.mismisimos.sweetdreams.model.Venta;

import java.util.List;

public interface VentaService {

    List<Venta> listAll();
    Venta save(Venta venta);
    Venta findById(int id);
    void delete(int id);

}
