package com.mismisimos.sweetdreams.service;

import com.mismisimos.sweetdreams.model.Venta;
import com.mismisimos.sweetdreams.repository.VentaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VentaServiceImplementation implements VentaService {

    @Autowired
    private VentaRepository ventaRepository;

    @Override
    public List<Venta> listAll() {
        return ventaRepository.findAll();
    }

    @Override
    public Venta save(Venta venta) {
        return ventaRepository.save(venta);
    }

    @Override
    public Venta findById(int id) {
        return ventaRepository.getOne(id);
    }

    @Override
    public void delete(int id) {
        ventaRepository.deleteById(id);
    }
}
