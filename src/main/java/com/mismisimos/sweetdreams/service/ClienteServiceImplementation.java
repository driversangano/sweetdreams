package com.mismisimos.sweetdreams.service;

import com.mismisimos.sweetdreams.exception.ClienteNotFoundException;
import com.mismisimos.sweetdreams.model.Cliente;
import com.mismisimos.sweetdreams.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteServiceImplementation implements ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    @Override
    public List<Cliente> listAll() {
        return clienteRepository.findAll();
    }

    @Override
    public Cliente save(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    @Override
    public Cliente findById(int id) {
        Optional<Cliente> cliente = clienteRepository.findById(id);
        if(cliente.isPresent()){
            return clienteRepository.findById(id).get();
        } else {
            throw new ClienteNotFoundException("Cliente no encontrado");
        }



    }

    @Override
    public void delete(int id) {
        clienteRepository.deleteById(id);
    }

    @Override
    public Cliente findByRut(String rut) {
        return clienteRepository.findByRut(rut);
    }
}
