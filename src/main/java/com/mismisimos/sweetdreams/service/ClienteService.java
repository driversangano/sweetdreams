package com.mismisimos.sweetdreams.service;


import com.mismisimos.sweetdreams.model.Cliente;

import java.util.List;

public interface ClienteService {
    List<Cliente> listAll();
    Cliente save(Cliente cliente);
    Cliente findById(int id);
    void delete(int id);
    Cliente findByRut(String rut);
}
