package com.mismisimos.sweetdreams.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class PedidoNotFoundException extends RuntimeException {
    /**
    *
    */
    private static final long serialVersionUID = -5159181697531418201L;

    public PedidoNotFoundException(final String message) {
        super(message);
    }
    
}