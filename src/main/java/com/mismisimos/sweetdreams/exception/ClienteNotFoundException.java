package com.mismisimos.sweetdreams.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ClienteNotFoundException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = -3471186334478822613L;

    public ClienteNotFoundException(String message) {
        super(message);
    }


}
