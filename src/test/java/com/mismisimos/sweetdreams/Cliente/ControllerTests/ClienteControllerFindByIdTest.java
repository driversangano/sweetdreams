package com.mismisimos.sweetdreams.Cliente.ControllerTests;

import com.mismisimos.sweetdreams.exception.ClienteNotFoundException;
import com.mismisimos.sweetdreams.model.Cliente;
import com.mismisimos.sweetdreams.service.ClienteService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureRestDocs(outputDir = "build/snippets")
@ExtendWith(MockitoExtension.class)
public class ClienteControllerFindByIdTest {

    @MockBean
    ClienteService clienteService;

    @Autowired
    MockMvc mockMvc;

    private Cliente cliente;

    @BeforeEach
    public void initClientes() {
        cliente = new Cliente();
        cliente.setId_Cliente(1);
        cliente.setNombre("Andres");
        cliente.setApellido("Grijalbo");
        cliente.setCorreo("andres@empresa.com");
        cliente.setDireccion("Calle 1 #564");
        cliente.setTelefono("+56968955895");
    }

    @Test //Error interno del servidor al no encontrar un cliente, Cambiar luego de manejar el error
    public void shouldReturnClienteNotFound() throws Exception {
        ClienteNotFoundException message = new ClienteNotFoundException("Cliente no encontrado");
        when(clienteService.findById(0)).thenThrow(message);

        this.mockMvc.perform(get("/clientes/0")).andExpect(status().isNotFound()).andDo(document("ClienteNotFound"));
    }

    @Test //Exito al obtener un cliente encontrado
    public void shouldReturnSuccess() throws Exception {
        when(clienteService.findById(1)).thenReturn(cliente);

        this.mockMvc.perform(get("/clientes/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id_Cliente").value(1))
                .andDo(document("ClienteFound"));
    }


}
